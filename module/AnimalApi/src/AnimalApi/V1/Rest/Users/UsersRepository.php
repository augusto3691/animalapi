<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AnimalApi\V1\Rest\Users;

/**
 * Description of UsersRepository
 *
 * @author HP
 */
class UsersRepository
{

    /**
     * @var \Zend\Db\TableGateway\TableGatewayInterface
     */
    private $tableGateway;

    public function __construct(\Zend\Db\TableGateway\TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function findAll()
    {
        $tableGateway = $this->tableGateway;
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbTableGateway($tableGateway);

        return new UsersCollection($paginatorAdapter);
    }

    public function findById($id)
    {
        $resultSet = $this->tableGateway->select(['id' => $id]);

        return $resultSet->current();
    }

}
