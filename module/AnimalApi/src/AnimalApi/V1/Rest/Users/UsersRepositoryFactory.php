<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AnimalApi\V1\Rest\Users;

class UsersRepositoryFactory implements \Zend\ServiceManager\FactoryInterface
{

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $adapter = $serviceLocator->get("DbAdapter");

//        $usersMapper = new UsersMapper();
        $hydrator = new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ClassMethods(), new UsersEntity());

        $tableGateway = new \Zend\Db\TableGateway\TableGateway('oauth_users', $adapter, NULL, $hydrator);
        $usersRepository = new UsersRepository($tableGateway);

        return $usersRepository;
    }

}
