<?php

return array(
    'router' => array(
        'routes' => array(
            'animal-api.rest.service-types' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/service_types[/:service_types_id]',
                    'defaults' => array(
                        'controller' => 'AnimalApi\\V1\\Rest\\ServiceTypes\\Controller',
                    ),
                ),
            ),
            'animal-api.rest.animal-types' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/animal_types[/:animal_types_id]',
                    'defaults' => array(
                        'controller' => 'AnimalApi\\V1\\Rest\\AnimalTypes\\Controller',
                    ),
                ),
            ),
            'animal-api.rest.animal-races' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/animal_races[/:animal_races_id]',
                    'defaults' => array(
                        'controller' => 'AnimalApi\\V1\\Rest\\AnimalRaces\\Controller',
                    ),
                ),
            ),
            'animal-api.rest.color-types' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/color_types[/:color_types_id]',
                    'defaults' => array(
                        'controller' => 'AnimalApi\\V1\\Rest\\ColorTypes\\Controller',
                    ),
                ),
            ),
            'animal-api.rest.users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users[/:users_id]',
                    'defaults' => array(
                        'controller' => 'AnimalApi\\V1\\Rest\\Users\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'animal-api.rest.service-types',
            1 => 'animal-api.rest.animal-types',
            2 => 'animal-api.rest.animal-races',
            3 => 'animal-api.rest.color-types',
            4 => 'animal-api.rest.users',
        ),
    ),
    'zf-rest' => array(
        'AnimalApi\\V1\\Rest\\ServiceTypes\\Controller' => array(
            'listener' => 'AnimalApi\\V1\\Rest\\ServiceTypes\\ServiceTypesResource',
            'route_name' => 'animal-api.rest.service-types',
            'route_identifier_name' => 'service_types_id',
            'collection_name' => 'service_types',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'AnimalApi\\V1\\Rest\\ServiceTypes\\ServiceTypesEntity',
            'collection_class' => 'AnimalApi\\V1\\Rest\\ServiceTypes\\ServiceTypesCollection',
            'service_name' => 'service_types',
        ),
        'AnimalApi\\V1\\Rest\\AnimalTypes\\Controller' => array(
            'listener' => 'AnimalApi\\V1\\Rest\\AnimalTypes\\AnimalTypesResource',
            'route_name' => 'animal-api.rest.animal-types',
            'route_identifier_name' => 'animal_types_id',
            'collection_name' => 'animal_types',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'AnimalApi\\V1\\Rest\\AnimalTypes\\AnimalTypesEntity',
            'collection_class' => 'AnimalApi\\V1\\Rest\\AnimalTypes\\AnimalTypesCollection',
            'service_name' => 'animal_types',
        ),
        'AnimalApi\\V1\\Rest\\AnimalRaces\\Controller' => array(
            'listener' => 'AnimalApi\\V1\\Rest\\AnimalRaces\\AnimalRacesResource',
            'route_name' => 'animal-api.rest.animal-races',
            'route_identifier_name' => 'animal_races_id',
            'collection_name' => 'animal_races',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'AnimalApi\\V1\\Rest\\AnimalRaces\\AnimalRacesEntity',
            'collection_class' => 'AnimalApi\\V1\\Rest\\AnimalRaces\\AnimalRacesCollection',
            'service_name' => 'animal_races',
        ),
        'AnimalApi\\V1\\Rest\\ColorTypes\\Controller' => array(
            'listener' => 'AnimalApi\\V1\\Rest\\ColorTypes\\ColorTypesResource',
            'route_name' => 'animal-api.rest.color-types',
            'route_identifier_name' => 'color_types_id',
            'collection_name' => 'color_types',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'AnimalApi\\V1\\Rest\\ColorTypes\\ColorTypesEntity',
            'collection_class' => 'AnimalApi\\V1\\Rest\\ColorTypes\\ColorTypesCollection',
            'service_name' => 'color_types',
        ),
        'AnimalApi\\V1\\Rest\\Users\\Controller' => array(
            'listener' => 'AnimalApi\\V1\\Rest\\Users\\UsersResource',
            'route_name' => 'animal-api.rest.users',
            'route_identifier_name' => 'users_id',
            'collection_name' => 'users',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'AnimalApi\\V1\\Rest\\Users\\UsersEntity',
            'collection_class' => 'AnimalApi\\V1\\Rest\\Users\\UsersCollection',
            'service_name' => 'users',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'AnimalApi\\V1\\Rest\\ServiceTypes\\Controller' => 'HalJson',
            'AnimalApi\\V1\\Rest\\AnimalTypes\\Controller' => 'HalJson',
            'AnimalApi\\V1\\Rest\\AnimalRaces\\Controller' => 'HalJson',
            'AnimalApi\\V1\\Rest\\ColorTypes\\Controller' => 'HalJson',
            'AnimalApi\\V1\\Rest\\Users\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'AnimalApi\\V1\\Rest\\ServiceTypes\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'AnimalApi\\V1\\Rest\\AnimalTypes\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'AnimalApi\\V1\\Rest\\AnimalRaces\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'AnimalApi\\V1\\Rest\\ColorTypes\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'AnimalApi\\V1\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'AnimalApi\\V1\\Rest\\ServiceTypes\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/json',
            ),
            'AnimalApi\\V1\\Rest\\AnimalTypes\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/json',
            ),
            'AnimalApi\\V1\\Rest\\AnimalRaces\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/json',
            ),
            'AnimalApi\\V1\\Rest\\ColorTypes\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/json',
            ),
            'AnimalApi\\V1\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.animal-api.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'AnimalApi\\V1\\Rest\\ServiceTypes\\ServiceTypesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.service-types',
                'route_identifier_name' => 'service_types_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'AnimalApi\\V1\\Rest\\ServiceTypes\\ServiceTypesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.service-types',
                'route_identifier_name' => 'service_types_id',
                'is_collection' => true,
            ),
            'AnimalApi\\V1\\Rest\\AnimalTypes\\AnimalTypesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.animal-types',
                'route_identifier_name' => 'animal_types_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'AnimalApi\\V1\\Rest\\AnimalTypes\\AnimalTypesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.animal-types',
                'route_identifier_name' => 'animal_types_id',
                'is_collection' => true,
            ),
            'AnimalApi\\V1\\Rest\\AnimalRaces\\AnimalRacesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.animal-races',
                'route_identifier_name' => 'animal_races_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'AnimalApi\\V1\\Rest\\AnimalRaces\\AnimalRacesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.animal-races',
                'route_identifier_name' => 'animal_races_id',
                'is_collection' => true,
            ),
            'AnimalApi\\V1\\Rest\\ColorTypes\\ColorTypesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.color-types',
                'route_identifier_name' => 'color_types_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'AnimalApi\\V1\\Rest\\ColorTypes\\ColorTypesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.color-types',
                'route_identifier_name' => 'color_types_id',
                'is_collection' => true,
            ),
            'AnimalApi\\V1\\Rest\\Users\\UsersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.users',
                'route_identifier_name' => 'users_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ClassMethods',
            ),
            'AnimalApi\\V1\\Rest\\Users\\UsersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animal-api.rest.users',
                'route_identifier_name' => 'users_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-apigility' => array(
        'db-connected' => array(
            'AnimalApi\\V1\\Rest\\ServiceTypes\\ServiceTypesResource' => array(
                'adapter_name' => 'DbAdapter',
                'table_name' => 'service_types',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'AnimalApi\\V1\\Rest\\ServiceTypes\\Controller',
                'entity_identifier_name' => 'id',
            ),
            'AnimalApi\\V1\\Rest\\AnimalTypes\\AnimalTypesResource' => array(
                'adapter_name' => 'DbAdapter',
                'table_name' => 'animal_types',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'AnimalApi\\V1\\Rest\\AnimalTypes\\Controller',
                'entity_identifier_name' => 'id',
            ),
            'AnimalApi\\V1\\Rest\\AnimalRaces\\AnimalRacesResource' => array(
                'adapter_name' => 'DbAdapter',
                'table_name' => 'animal_races',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'AnimalApi\\V1\\Rest\\AnimalRaces\\Controller',
                'entity_identifier_name' => 'id',
            ),
            'AnimalApi\\V1\\Rest\\ColorTypes\\ColorTypesResource' => array(
                'adapter_name' => 'DbAdapter',
                'table_name' => 'color_types',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'AnimalApi\\V1\\Rest\\ColorTypes\\Controller',
                'entity_identifier_name' => 'id',
            ),
        ),
    ),
    'zf-content-validation' => array(
        'AnimalApi\\V1\\Rest\\AnimalTypes\\Controller' => array(
            'input_filter' => 'AnimalApi\\V1\\Rest\\AnimalTypes\\Validator',
        ),
        'AnimalApi\\V1\\Rest\\AnimalRaces\\Controller' => array(
            'input_filter' => 'AnimalApi\\V1\\Rest\\AnimalRaces\\Validator',
        ),
        'AnimalApi\\V1\\Rest\\ColorTypes\\Controller' => array(
            'input_filter' => 'AnimalApi\\V1\\Rest\\ColorTypes\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'AnimalApi\\V1\\Rest\\AnimalTypes\\Validator' => array(
            0 => array(
                'name' => 'type',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'active',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(),
            ),
            2 => array(
                'name' => 'modify_date',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            3 => array(
                'name' => 'create_date',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'AnimalApi\\V1\\Rest\\AnimalRaces\\Validator' => array(
            0 => array(
                'name' => 'race',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'active',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(),
            ),
            2 => array(
                'name' => 'modify_date',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            3 => array(
                'name' => 'create_date',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'AnimalApi\\V1\\Rest\\ColorTypes\\Validator' => array(
            0 => array(
                'name' => 'color',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'active',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(),
            ),
            2 => array(
                'name' => 'modify_date',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            3 => array(
                'name' => 'create_date',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'AnimalApi\\V1\\Rest\\Users\\UsersResource' => 'AnimalApi\\V1\\Rest\\Users\\UsersResourceFactory',
            'AnimalApi\\V1\\Rest\\Users\\UsersRepository' => 'AnimalApi\\V1\\Rest\\Users\\UsersRepositoryFactory',
        ),
    ),
);
